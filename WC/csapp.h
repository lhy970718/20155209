#include "csapp.h"

#define RIO_BUFSIZE 8192
typedef struct{
  int rio_fd;
  int rio_cnt;
  char *rio_bufptr;
  char rio_buf[RIO_BUFSIZE];
}rio_t;

void echo(int connfd);

int open_clientfd(char *hostname,int port);
int open_listenfd(int port);

ssize_t rio_writen(int fd,void *usrbuf,size_t n);
ssize_t rio_readinitb(rio_t *rp,int fd);
ssize_t rio_readlined(rio_t *rp,void *usrbuf,size_t maxlen);

