#include<stdio.h>
#include<stdlib.h>
void tx(char *a)
{
    FILE *fp;
    char ch;
    printf("16进制输出为：\n");
    fp=fopen(a,"r");
    ch=fgetc(fp);
    int count=0;
    while(ch!=EOF)
    {
        if(ch=='\n') 
            printf("  0a");
        else
            printf("%4x",ch);
	count++;
	if(count==16)
	{
		printf("\n");
		count=0;
	}
        ch=fgetc(fp);
    }
    fclose(fp);
}
