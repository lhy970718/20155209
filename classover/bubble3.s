	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 10
	.globl	_bubble_c
	.align	4, 0x90
_bubble_c:                              ## @bubble_c
	.cfi_startproc
## BB#0:
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	movq	%rdi, -8(%rbp)
	movl	%esi, -12(%rbp)
	movl	$1, -20(%rbp)
LBB0_1:                                 ## =>This Loop Header: Depth=1
                                        ##     Child Loop BB0_3 Depth 2
	movl	-20(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jge	LBB0_11
## BB#2:                                ##   in Loop: Header=BB0_1 Depth=1
	movl	-20(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -16(%rbp)
LBB0_3:                                 ##   Parent Loop BB0_1 Depth=1
                                        ## =>  This Inner Loop Header: Depth=2
	cmpl	$0, -16(%rbp)
	jl	LBB0_9
## BB#4:                                ##   in Loop: Header=BB0_3 Depth=2
	movq	-8(%rbp), %rax
	movslq	-16(%rbp), %rcx
	movl	(%rax,%rcx,4), %edx
	movl	%edx, -24(%rbp)
	movq	-8(%rbp), %rax
	movslq	-16(%rbp), %rcx
	movl	4(%rax,%rcx,4), %edx
	movl	%edx, -28(%rbp)
	movl	-28(%rbp), %edx
	cmpl	-24(%rbp), %edx
	jge	LBB0_6
## BB#5:                                ##   in Loop: Header=BB0_3 Depth=2
	movl	-28(%rbp), %eax
	movl	%eax, -32(%rbp)         ## 4-byte Spill
	jmp	LBB0_7
LBB0_6:                                 ##   in Loop: Header=BB0_3 Depth=2
	movl	-24(%rbp), %eax
	movl	%eax, -32(%rbp)         ## 4-byte Spill
LBB0_7:                                 ##   in Loop: Header=BB0_3 Depth=2
	movl	-32(%rbp), %eax         ## 4-byte Reload
	movq	-8(%rbp), %rcx
	movslq	-16(%rbp), %rdx
	movl	%eax, (%rcx,%rdx,4)
	movl	-24(%rbp), %eax
	movq	-8(%rbp), %rcx
	movslq	-16(%rbp), %rdx
	movl	%eax, 4(%rcx,%rdx,4)
## BB#8:                                ##   in Loop: Header=BB0_3 Depth=2
	movl	-16(%rbp), %eax
	addl	$4294967295, %eax       ## imm = 0xFFFFFFFF
	movl	%eax, -16(%rbp)
	jmp	LBB0_3
LBB0_9:                                 ##   in Loop: Header=BB0_1 Depth=1
	jmp	LBB0_10
LBB0_10:                                ##   in Loop: Header=BB0_1 Depth=1
	movl	-20(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -20(%rbp)
	jmp	LBB0_1
LBB0_11:
	popq	%rbp
	retq
	.cfi_endproc


.subsections_via_symbols
