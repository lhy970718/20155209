void bubble_c(int *data,int count)
{
    int i , next;
    int pre_ele,next_ele;
    for(next = 1;next < count;next++)
    {
        for(i = next -1;i >= 0;i--)
        {
            pre_ele = *(data + i);
            next_ele = *(data + i + 1);
            *(data + i) = next_ele < pre_ele ? next_ele : pre_ele;
            *(data + i + 1) = pre_ele;
        }
    }
}
