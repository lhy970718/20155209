void bubble_a(int *data, int count){  
    int i,next;  
    for(next = 1; next < count; next++){  
        for(i = next - 1; i >= 0; i--)  
            if(*(data + i + 1) < *(data + i)){  
                int t = *(data + i + 1);  
                *(data + i + 1) = *(data + i);  
                *(data + i) = t;  
            }  
    }  
}
void main()
{
    int data[5]={5,5,2,0,9};
    int i;
    bubble_a(data,5);
    for(i=0;i<5;i++)
    {
        printf("%2d",data[i]);
    }
}   
