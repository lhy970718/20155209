#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define MAXLINE 256

int main(){

    char pwd[MAXLINE];

    getcwd(pwd,MAXLINE);

    puts(pwd);

    return 0;
}
