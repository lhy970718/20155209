#include <sys/stat.h>  
#include <unistd.h>  
#include <stdio.h>  
#define N 50  
main() {  
    struct stat buf;
    char filename[N];
    printf("input file name\n");
    scanf("%s",filename);  
    if(stat(filename, &buf)==-1)
      exit(0);
    printf("the %s size = %d\n", filename,buf.st_size);  
}
